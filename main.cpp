#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#define MAX_LESSONS 10
#define MAX_MARKS 31
static int total_lessons_counter = 0;
class Lesson { // main lessons class
public:
  std::vector<int> marks; // all marks you have
  std::string name;       // name of the lessons
  double mean;            // all marks / sizeof(marks)

  void add_name() { // add a name to the lesson
    std::cout << "Enter Lesson's name: ";
    std::cin >> name;
    total_lessons_counter++;
  }
  void add_mark() { // add marks to the lesson
    int choice;
    std::cout << "How many marks would you like to add? ";
    std::cin >> choice;
    system("clear");
    int mark;
    if (choice > 1) { // write a lot of nums
      for (int i = 0; i < choice; i++) {
        std::cin >> mark;
        marks.push_back(mark);
      }
    } else { // write one num
      int lol;
      std::cout << "Enter mark: ";
      std::cin >> lol;
      marks.push_back(lol);
    }
  }
  double get_mean() { // check mean definition
    double total_marks = 0;
    for (int i = 0; i < marks.size(); i++) {
      total_marks += marks.at(i);
    }
    mean = total_marks / marks.size();
    return mean;
  }
  void print_all_lessons_marks() {
    std::cout << "Marks: ";
    for (int i = 0; i < marks.size(); i++) { // print all marks in marks vector
      std::cout << marks.at(i) << " ";
    }
    std::cout << std::endl;
  }
};
int get_lesson_index(Lesson lessons[]) {
  int index;
  std::cout << "Choose a lesson:" << std::endl;
  for (int i = 0; i < total_lessons_counter; i++) {
    std::cout << i << " - " << lessons[i].name << std::endl;
  }
  std::cin >> index;
  if (index > total_lessons_counter or index < 0) {
    std::cout << "Index is incorrect";
    return -1;
  }
  return index;
}
void read_file(
    Lesson lessons[]) { // read marks from file and write them to vector
  std::string str;
  std::ifstream file("marks.txt");
  int i = 0;
  if (file.is_open()) {
    while (std::getline(file, str)) {
      std::istringstream iss(str);
      std::string lesson_name;
      int mark;
      iss >> lesson_name;
      while (iss >> mark) {
        lessons[i].marks.push_back(mark);
      }
      lessons[i].name = lesson_name;
      i++;
    }
  }
  total_lessons_counter = i;
  file.close();
}
void check_for_bad_marks(Lesson lessons[]) { // check if marks are bad
  for (int i = 0; i < total_lessons_counter; i++) {
    if ((lessons[i].mean = lessons[i].get_mean()) <= 3.5) {
      std::cout << "Your performance in subject " << lessons[i].name
                << " is slipping"
                << " your mean is: " << lessons[i].get_mean() << std::endl;
    } else {
      continue;
    }
  }
  std::cout << std::endl;
}
void write_file(
    Lesson lessons[]) { // write all data (names and marks) to the file
  std::ofstream file("marks.txt");
  if (file.is_open()) {
    for (int i = 0; i < total_lessons_counter; i++) {
      file << lessons[i].name;
      for (const auto &value : lessons[i].marks) {
        file << value << " ";
      }
      file << std::endl;
    }
  }
  file.close();
}
void print_all_marks(Lesson lessons[]) {
  for (int i = 0; i < total_lessons_counter; i++) {
    std::cout << i << " - " << lessons[i].name << ": ";
    for (const auto &elem : lessons[i].marks) {
      std::cout << elem << " ";
    }
    std::cout << std::endl << std::endl;
  }
}
int main() {
  Lesson lessons[MAX_LESSONS];
  char ch;
  while (true) {
    std::cout << "Choose an option:" << std::endl;
    std::cout << "1 - Add mark" << std::endl;
    std::cout << "2 - Add name" << std::endl;
    std::cout << "3 - Print all marks" << std::endl;
    std::cout << "4 - Print mean" << std::endl;
    std::cout << "5 - Write to file" << std::endl;
    std::cout << "6 - Read from file" << std::endl;
    std::cout << "7 - Print all Lesson's marks" << std::endl;
    std::cout << "0 - Exit" << std::endl;
    std::cin >> ch;
    switch (ch) {
    case '1':
      lessons[get_lesson_index(lessons)].add_mark();
      system("clear");
      break;
    case '2':
      lessons[total_lessons_counter].add_name();
      system("clear");
      break;
    case '3':
      system("clear");
      print_all_marks(lessons);
      break;
    case '4':
      std::cout << "Mean: " << lessons[get_lesson_index(lessons)].get_mean()
                << std::endl
                << std::endl;
      break;
    case '5':
      system("clear");
      write_file(lessons);
      system("clear");
      break;
    case '6':
      system("clear");
      read_file(lessons);
      system("clear");
      check_for_bad_marks(lessons);
      break;
    case '7':
      system("clear");
      lessons[get_lesson_index(lessons)].print_all_lessons_marks();
    case '0':
      return 0;
      break;
    }
  }
  return 0;
}
